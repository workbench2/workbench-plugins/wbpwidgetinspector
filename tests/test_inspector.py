from wbBase.application import App
from wbBase.applicationInfo import ApplicationInfo, PluginInfo

appinfo = ApplicationInfo(
    Plugins=[PluginInfo(Name="widgetinspector", Installation="default")]
)

def test_inspector():
    app = App(test=True, info=appinfo)
    assert "widgetinspector" in app.pluginManager
    app.Destroy()
