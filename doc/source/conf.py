# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html
import os
import sys
import wx
import wbBase
app = wbBase.App()
app.SetAssertMode(wx.APP_ASSERT_SUPPRESS)
import wbpWidgetinspector
sys.path.insert(0, os.path.abspath('../../Lib/wbpWidgetinspector'))

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'wbpWidgetinspector'
copyright = '2013-2023, Andreas Eigendorf'
author = 'Andreas Eigendorf'

version = wbpWidgetinspector.__version__
release = wbpWidgetinspector.__version__

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.viewcode',
]

templates_path = ['_templates']
exclude_patterns = []

language = 'en'

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "sphinx_rtd_theme"
html_show_sourcelink = False
html_show_sphinx = False
html_static_path = ['_static']

add_module_names = False

autodoc_default_options = {
    "members": True,
    "show-inheritance": True,
    "inherited-members": False,
}
autodoc_class_signature = "separated"
autodoc_member_order = "bysource"
autodoc_typehints = "description"
autodoc_typehints_format = "short"
autodoc_preserve_defaults = True
