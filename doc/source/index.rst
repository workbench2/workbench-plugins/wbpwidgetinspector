
Welcome to the wbpWidgetinspector documentation
===============================================================================

.. figure:: _static/widgetinspector.png
   :scale: 100 %
   :alt: Widgetinspector Panel screenshot

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   wbpWidgetinspector


Indices and tables
-------------------------------------------------------------------------------

* :ref:`genindex`
* :ref:`modindex`
