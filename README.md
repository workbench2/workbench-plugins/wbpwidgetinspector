# wbpWidgetinspector

Widgetinspector plugin for Workbench applications. This plugin allows you to 
launch the wx Widgetinspector from within your running application.

## Installation

```shell
pip install wbpWidgetinspector
```

## Documentation

For details read the [Documentation](https://workbench2.gitlab.io/workbench-plugins/wbpwidgetinspector/).